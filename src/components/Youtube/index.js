import React from "react";
import { Paper, FormControl, FormLabel, TextField } from "@material-ui/core";
import { useNode } from "@craftjs/core";

const Youtube = ({ videoID = "IlzzD-dHJ1g", padding = 20 }) => {
  const {
    connectors: { connect, drag }
  } = useNode();

  const videoSrc = `https://www.youtube.com/embed/${videoID}?autoplay=0&rel=0&modestbranding=1`;
  return (
    <Paper
      ref={ref => connect(drag(ref))}
      style={{ margin: "5px 0", padding: `${padding}px`, height: 400 }}
    >
      <iframe
        className="player"
        type="text/html"
        width="100%"
        height="100%"
        src={videoSrc}
        frameBorder="0"
      />
    </Paper>
  );
};

export const YoutubeSettings = () => {
  const { setProp, props } = useNode(node => ({
    props: node.data.props
  }));
  return (
    <div>
      <FormControl fullWidth={true} margin="normal" component="fieldset">
        <FormLabel component="legend">Video ID</FormLabel>
        <TextField
          defaultValue={props.videoID}
          id="standard-basic"
          onChange={e => setProp(props => (props.videoID = e.target.value))}
        />
      </FormControl>
    </div>
  );
};

Youtube.craft = {
  related: {
    settings: YoutubeSettings
  }
};

export default Youtube;
