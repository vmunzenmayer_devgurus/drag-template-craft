import React from "react";
import { Paper, FormControl, FormLabel, TextField } from "@material-ui/core";
import { useNode } from "@craftjs/core";

const Image = ({
  url = "https://s3.amazonaws.com/sidramedicalsupply/site/sidra_1565678246_sidra-medical-supply.png",
  padding = 20
}) => {
  const {
    connectors: { connect, drag }
  } = useNode();

  return (
    <Paper
      ref={ref => connect(drag(ref))}
      style={{ margin: "5px 0", padding: `${padding}px` }}
    >
      <img src={url} />
    </Paper>
  );
};

export const ImageSettings = () => {
  const { setProp, props } = useNode(node => ({
    props: node.data.props
  }));
  return (
    <div>
      <FormControl fullWidth={true} margin="normal" component="fieldset">
        <FormLabel component="legend">URL</FormLabel>
        <TextField
          defaultValue={props.videoID}
          id="standard-basic"
          onChange={e => setProp(props => (props.url = e.target.value))}
        />
      </FormControl>
    </div>
  );
};

Image.craft = {
  related: {
    settings: ImageSettings
  }
};

export default Image;
