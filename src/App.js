import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { CardContent, Grid, Divider, Card, Paper } from "@material-ui/core";
import { Editor, Frame, Canvas } from "@craftjs/core";

import ToolBox from "./components/ToolBox";
import SettingPanel from "./components/SettingPanel";
import TopBar from "./components/TopBar";
import Wrapper from "./components/Wrapper";
import Buttonja from "./components/Button";
import { CardTop, CardBottom } from "./components/Card";
import Text from "./components/Text";

import "./App.css";

const useStyles = makeStyles(theme => ({
  h3: {
    color: "#4B597A",
    marginBottom: "5px"
  },
  app: {
    backgroundColor: "#F6F5F8"
  },
  formatCard: {
    backgroundColor: "#F0EFF2",
    padding: "20px",
    marginTop: "15px",
    cursor: "pointer"
  },
  grid: {
    padding: "20px"
  },
  small: {
    color: "#CDCDCD",
    marginBottom: "15px",
    marginUp: "15px"
  },
  root: {
    flexGrow: 1
  },
  paper: {
    //padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

function App() {
  const classes = useStyles();

  return (
    <div className={classes.app}>
      <React.Fragment>
        <Editor>
          <Grid container spacing={2} className={classes.grid}>
            {/* Layout Draggable */}
            <Grid item xs={3}>
              <Card>
                <CardContent>
                  <h3 className={classes.h3}>Format</h3>
                  <div className={classes.small}>
                    Please drag and drop to layout your components
                  </div>
                  <Divider />
                  <ToolBox />
                  <SettingPanel />
                </CardContent>
              </Card>
            </Grid>
            {/* Layout Droppable */}

            <Grid item xs={9}>
              <Card>
                <CardContent>
                  <h3 className={classes.h3}>Layout</h3>
                  <div className={classes.small}>
                    This is how your content will be displayed.
                  </div>
                  <Divider />
                  <br />
                  <div className={classes.root}>
                    <Frame>
                      <Canvas id="layout">
                        <Grid container spacing={3}>
                          <Grid item xs={12}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="header"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row1"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row2"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={3}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row3"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={3}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row4"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={3}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row5"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={3}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="row6"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                          <Grid item xs={12}>
                            <Paper className={classes.paper}>
                              <Canvas
                                is={Wrapper}
                                id="footer"
                                padding={20}
                                background="#DCDCDC"
                              ></Canvas>
                            </Paper>
                          </Grid>
                        </Grid>
                      </Canvas>
                    </Frame>
                  </div>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Editor>
      </React.Fragment>
    </div>
  );
}

export default App;
